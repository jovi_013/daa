import random, time

items = {
    0: (30, 100),  1: (28, 100),   2: (35, 1000), 3: (15, 600),
    4: (29, 300),  5: (17, 500),   6: (14, 100),  7: (24, 800),
    8: (27, 300),  9: (10, 900),  10: (16, 150), 11: (11, 200),
    12: (12, 150), 13: (13, 850), 14: (28, 350), 15: (10, 650),
    16: (30, 1950), 17: (28, 550), 18: (25, 150), 19: (25, 300),
}

def GA(length, capacity, iters, n):
    # Initial
    chromosomes = [''.join([str(random.randint(0, 1)) for i in range(n)]) for j in range(n)]
    population = len(chromosomes)
    
    for j in range(iters):
        # Selection
        winner = []
        for i in range(population):
            chrom1, chrom2 = random.choice(chromosomes), random.choice(chromosomes)
            winner.append(chrom1 if fitness_function(chrom1, capacity) > fitness_function(chrom2, capacity) else chrom2)

        # Crossover
        crossed = []
        for i in range(population // 2):
            chrom1, chrom2 = random.choice(winner), random.choice(winner)
            crossed1, crossed2 = chrom1[:16] + chrom2[-4:], chrom2[:16] + chrom1[-4:]
            crossed.append(crossed1)
            crossed.append(crossed2)

        # Mutation
        idx1, idx2 = get_2_random_index(length)
        mutated = []
        for i in range(population):
            if random.randint(1,100) <= 20:
                mutated_chrom = list(crossed[i])
                for idx in [idx1, idx2]:
                    mutated_chrom[idx] = '1' if mutated_chrom[idx] == '0' else '0'
                mutated.append(''.join(mutated_chrom))
            else:
                mutated.append(crossed[i])
        
        chromosomes = mutated
    
    best_chrom = ''.join(best(chromosomes, capacity))
    # print(fitness_function(best_chrom, capacity))
    return best_chrom, fitness_function(best_chrom, capacity)
    

def fitness_function(chromosome, capacity, algorithm='GA'):
    total_importance, total_weight = 0, 0

    for i in range(len(chromosome)):
        cell = int(chromosome[i]) if algorithm == 'GA' else round(chromosome[i])
        importance, weight = items[i]
    
        total_importance += cell * importance
        total_weight += cell * weight
    
    return 0 if total_weight > capacity else total_importance


def best(chromosomes, capacity):
    best_chrom = chromosomes[0]
    best_fitness = fitness_function(best_chrom, capacity)

    for i in range(1, len(chromosomes)):
        current_chrom = chromosomes[i]
        current_fitness = fitness_function(current_chrom, capacity)

        if current_fitness > best_fitness:
            best_fitness = current_fitness
            best_chrom = current_chrom

    return best_chrom

def get_2_random_index(length):
    idx1, idx2 = random.randrange(length), random.randrange(length)
    while idx2 == idx1: idx2 = random.randrange(length)
    return idx1, idx2


# --------------------------------------------------------------------

def FGA(length, capacity, iters, blueprint, glr, dr, ilr, n):
    # Initial

    blueprint = [random.randint(0,10) / 10 for i in range(length)]

    chromosomes = [blueprint for i in range(n)]
    population = len(chromosomes)
    
    for j in range(iters):
        # Selection
        winner = []
        for i in range(population):
            chrom1, chrom2 = random.choice(chromosomes), random.choice(chromosomes)
            winner.append(chrom1 if fitness_function(chrom1, capacity, 'FGA') > fitness_function(chrom2, capacity, 'FGA') else chrom2)

        # Create individual
        encoded_chromosomes = []
        for i in range(population):
            chrom1, chrom2 = random.choice(winner), random.choice(winner)
            winner[i], individual = born_an_individual(length, blueprint, chrom1, chrom2, glr, dr)
            encoded_chromosomes.append(individual)

        # Crossover
        crossed_chromosomes = []
        for i in range(population):
            idx1, idx2 = get_2_random_index(length-1)

            chrom1, chrom2 = winner[idx1], winner[idx2]
            binary_chrom1, binary_chrom2 = encoded_chromosomes[idx1], encoded_chromosomes[idx2]
            
            tmp1, tmp2 = get_2_random_index(length)
            s1, s2 = min(tmp1, tmp2), max(tmp1, tmp2)

            decider = random.randint(0, 1)
            if decider == 1:
                crossed = chrom1[:s1] + chrom2[s1:s2] + chrom1[s2:]
                crossed_binary = binary_chrom1[:s1] + binary_chrom2[s1:s2] + binary_chrom1[s2:]
            else:
                crossed = chrom2[:s1] + chrom1[s1:s2] + chrom2[s2:]
                crossed_binary = binary_chrom2[:s1] + binary_chrom1[s1:s2] + binary_chrom2[s2:]

            for k, idx in enumerate(crossed_binary):
                if k == 1:
                    crossed[idx] += ilr
                    crossed[idx] = 1 if crossed[idx] > 1 else crossed[idx]
                elif k == 0:
                    crossed[idx] -= ilr
                    crossed[idx] = 0 if crossed[idx] < 0 else crossed[idx]

            crossed_chromosomes.append(crossed)

        # New generation blueprint
        all_chromosomes = []
        for i in crossed_chromosomes:
            all_chromosomes += i
        avg = sum(all_chromosomes) / len(all_chromosomes)
        blueprint = [avg for k in range(length)]
    
    best_chrom = best(crossed_chromosomes, capacity)
    # print(fitness_function(best_chrom, capacity, 'FGA'))
    best_fitness_value = fitness_function(best_chrom, capacity, 'FGA')
    best_chrom = ''.join([str(round(best_chrom[i])) for i in range(length)])
    
    return best_chrom, best_fitness_value


def born_an_individual(length, blueprint, chromosome1, chromosome2, glr, dr):
    encoded = []
    
    indiv = born_a_child(chromosome1, chromosome2, blueprint, glr, dr)
    for cell in indiv :
        encoded.append(1 if random.randint(1,100) / 100 <= cell else 0)

    return indiv, encoded


def born_a_child(PVC1, PVC2, PVB, glr, dr) :
    PVC2_copy = PVC2.copy()
    PVC2_copy.sort()
    EPVi = 0
    EPV = []

    for i in range(len(PVC1)) :
        idx = PVC2.index(PVC2_copy[-(i+1)])

        if glr * PVB[idx] + (1-glr) * PVC1[idx] < dr:
            EPVi = dr
        elif glr * PVB[idx] + (1-glr) * PVC1[idx] > (1 - dr):
            EPVi = 1-dr
        else:
            EPVi = glr * PVB[idx] + (1 - glr) * PVC1[idx]

        EPV.append(EPVi)

    return EPV

# print(GA(20, 5000, 5, 20))
# print(FGA(20, 5000, 5, None, 0.07, 0.2, 0.2, 20))



accuracy_GA = []
time_start_GA = time.time()

for i in range(100):
    _, fitness_value = GA(20, 5000, 5, 20)
    accuracy_GA.append(fitness_value)

time_end_GA = time.time()

avg_accuracy_GA = sum(accuracy_GA) / len(accuracy_GA)

# ------------------------------------------------------------------

accuracy_FGA = []
time_start_FGA = time.time()

for i in range(100):
    _, fitness_value = FGA(20, 5000, 5, None, 0.1, 0.2, 0.2, 20)
    accuracy_FGA.append(fitness_value)

time_end_FGA = time.time()

avg_accuracy_FGA = sum(accuracy_FGA) / len(accuracy_FGA)

print("GA:" )
print("- Time:", time_end_GA - time_start_GA)
print("- Average fitness value:", avg_accuracy_GA)
print('--------------------------------------------')
print("FGA:" )
print("- Time:", time_end_FGA - time_start_FGA)
print("- Average fitness value:", avg_accuracy_FGA)
